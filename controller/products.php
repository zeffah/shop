<?php
/**
 *
 */
class ProductController {
  private $app;
  private $conn = null;
  private $productAPI;
  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->productAPI = new ProductAPI($this->app);
  }

  function getAllProducts($req, $res, $args){
    $data = $this->productAPI->listAllProducts();
    $products = json_decode($data);
    return $this->app->view->render($res, 'products/products_list.html', ['products'=> $products->products]);
  }

  //new product controller
  function createNewProduct($req, $res, $args) {
    $data = $this->productAPI->createNewProduct(json_encode($req->getParsedBody()));
    $response = json_decode($data);
    $success = $response->success;
    if($success == 1){
      $msg = $response->message;
       $this->app->flash->addMessage('product_create', $msg);
    }else{
      $error = $response->message;
       $this->app->flash->addMessage('product_create_error', $error);
    }
    return $res->withStatus(302)->withHeader('Location', '/products/createNew');
  }

//new product view
  function newView($req, $res, $args) {
      return $this->app->view->render($res, 'products/createNew.html');
  }
}


// Notice: Array to string conversion in /home/zeffah/Documents/projects/php/shop/controller/inventory.php on line 31
// Array
// Notice: Trying to get property 'products' of non-object in /home/zeffah/Documents/projects/php/shop/controller/inventory.php on line 33

// High Yield Dairy Meal
// Notice: Trying to get property 'products' of non-object in /home/zeffah/Documents/projects/php/shop/controller/inventory.php on line 33


 ?>
