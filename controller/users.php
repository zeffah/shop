<?php
class UsersController {
  private $userAPI;
  private $app;
  private $conn = null;

  function __construct($app) {
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->userAPI = new UserAPI($this->app);
  }

  function login($req, $res, $args) {
    if($req->getAttribute('has_errors')){
    //There are errors, read them
    $errors = $req->getAttribute('errors');
    $this->app->flash->addMessage('login_error', $errors);
    // $errors = json_encode($errors);
    return json_encode($errors);
    // return $res->withStatus(302)->withHeader('Location', '/users/userlogin');
  }
    $data = $this->userAPI->userAunthenticate(json_encode($req->getParsedBody()));
    $response = json_decode($data);
    $success = $response->success;
    if ($success == 1) {

      $_SESSION['slimFlash']['username'] = $response->user->username;
      $_SESSION['slimFlash']['password'] = $response->user->password;

      // print_r($_SESSION['slimFlash']['username']);
      return $res->withRedirect('/dashboard');
       // return json_encode($response->user);
    }else {
      $error = $response->message;
       $this->app->flash->addMessage('login_error', $error);
       return $res->withStatus(302)->withHeader('Location', '/users/userlogin');
    }
  }

  function register($req, $res, $args) {
      $data = $this->userAPI->userCreate(json_encode($req->getParsedBody()));
      $response =  json_decode($data);

      $error = $response->message;
      $this->app->flash->addMessage('user_register', $response);
        return $res->withStatus(302)->withHeader('Location', '/users/user-register');
  }

  //get login view
    function userLogin($req, $res) {
      return $this->app->view->render($res, 'user/login.html', ['name'=>'zeffah']);
    }

  //get regster view
    function userRegister($req, $res) {
      $sql = "SELECT * FROM shop";
      $stmt = $this->conn->prepare($sql);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $this->app->view->render($res, 'user/signup.html', ['shops'=>$rows]);
    }

    //get all users view
  function getAllUsers($req, $res) {
    $allUsers = null;
    try {
      $sql = "SELECT * FROM staff ORDER BY id ASC";
      // $db = new Database();
      // $conn = $db->connect();
      // $conn = $this->app->db;
      $stmt = $this->conn->prepare($sql);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $this->app->view->render($res, 'user/users.html', ['staff'=>$rows]);
      // return $res->withJson($rows);
    } catch (PDOException $e) {
      return $res->withJson($e->getMessage());
    }
  }
}

 ?>
