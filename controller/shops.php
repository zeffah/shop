<?php
class ShopsController {
  private $app = null;
  private $conn = null;
  private $shopsAPI = null;
  private $salesAPI = null;
  private $inventoryAPI = null;
function  __construct($app) {
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->shopsAPI = new ShopsAPI($this->app);
    $this->salesAPI = new SalesAPI($this->app);
    $this->inventoryAPI = new InventoryAPI($this->app);
  }

  function getAllShops($req, $res, $args){
    $data = $this->shopsAPI->getAllShops();
    $response = json_decode($data);
    $this->app->view->render($res, 'shops/shops.html', ['shops'=>$response]);
  }

  function addShop($req, $res){
    $this->app->view->render($res, 'shops/add_shop.html', ['shop'=>"Another shop"]);
  }

  function shopInfo($req, $res, $args){
    $shopId = $args['shopId'];
    $invData = $this->inventoryAPI->listShopInventories($shopId);
    $shopData = $this->shopsAPI->getShopInfo($shopId);
    $salesData = json_decode($this->salesAPI->getShopSales($shopId));
    $shops = json_decode($this->shopsAPI->getAllShops());
    $info = json_decode($shopData);
    $inventories = json_decode($invData);
    $inventoryValue;
    foreach ($inventories->inventories as $inventory => $value) {
      $inventoryValue =+ ($value->quantity) * ($value->buying_price);
    }
    $this->app->view->render($res, 'shops/shop_info.html', ['shopInfo'=>$info, 'inventory'=>$inventories, 'shops'=>$shops, 'sales'=>$salesData, 'inventoryValue'=> $inventoryValue]);
  }

  function createNew($req, $res, $args){
    $data = $this->shopsAPI->addShop(json_encode($req->getParsedBody()));
    $response = json_decode($data);
    $success = $response->success;
    if ($success == 1) {
       return $res->withRedirect('/shops');
    }else {
      $error = $response->message;
       $this->app->flash->addMessage('login_error', $error);
       return $res->withStatus(302)->withHeader('Location', '/shops/addShop');
    }
  }
}

 ?>
