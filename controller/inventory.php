<?php
/**
**My inventory controller
**/
class inventoryController {
  private $app;
  private $conn = null;
  private $inventoryAPI;
  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->inventoryAPI = new InventoryAPI($this->app);
  }

  function getInventoryList($req, $res, $args) {
    $shopId = $args['shop_id'];
    $response = $this->inventoryAPI->listShopInventories($shopId);
    return $response;
  }

  function createInventory($req, $res, $args){
    $data = $this->inventoryAPI->createNewInventory(json_encode($req->getParsedBody()));
    $response = json_decode($data);
    $success = $response->success;
    if($success == 1){
      $msg = $response->message;
       $this->app->flash->addMessage('inventory_create', $msg);
    }else{
      $error = $response->message;
       $this->app->flash->addMessage('inventory_create_error', $error);
    }
    return $res->withStatus(302)->withHeader('Location', '/inventories/add');
  }

  function editInventory($req, $res, $args){
    $body = json_decode(json_encode($req->getParsedBody()));
    $request = json_encode(array('quantity'=> $body->quantity, 'id'=> $body->id, 'existing_quantity'=>$body->existing_quantity));
    $response = json_decode($this->inventoryAPI->editInventory($request));
    $id = $response->id;
    $shopId = $body->shop_id;
    $success = $response->success;
    if($success == 1){
      $this->app->flash->addMessage('inventory_edit', $response->message);
    }else {
      $this->app->flash->addMessage('inventory_edit_error', $response->message);
    }
    return $res->withStatus(302)->withHeader('Location', "/shops/$shopId");
  }

  function deleteInventory($req, $res, $args){
    // return json_encode($req->getParsedBody());
    $data = $req->getParsedBody();
    $body = json_encode($data);
    $response = json_decode($this->inventoryAPI->deleteInventory($body));
    $id = $response->id;
    $shopId = json_decode($body)->shop_id;
    $success = $response->success;
    if($success == 1){
      $this->app->flash->addMessage('inventory_delete', $response->message);
    }else {
      $this->app->flash->addMessage('inventory_delete_error', $response->message);
    }
    return $res->withStatus(302)->withHeader('Location', "/shops/$shopId");
  }

  //create view
  function createView($req, $res, $args)  {
    $productAPI = new ProductAPI($this->app);
    $shopsAPI = new ShopsAPI($this->app);
    $products = json_decode($productAPI->listAllProducts());
    $shops = json_decode($shopsAPI->getAllShops());
    return $this->app->view->render($res, 'inventory/create_new.html', ['items'=>['products'=>$products->products, 'shops'=>$shops->shops]]);
  }

//show all view
  function showAllView($req, $res, $args)  {
    $shopId = $args['shop_id'];
    $response = null;
    if (!is_numeric($shopId)) {
      $response = json_encode(array('success'=> 0, 'error'=>inventoryAPI::INVENTORY_LIST_INVALID_SHOP_ID, 'message'=>'Invalid shop id. Required number, given String'));
    }else {
      $response = json_decode($this->inventoryAPI->listShopInventories($shopId));
    }
    return $this->app->view->render($res, 'inventory/inventories.html', ['inventories'=>$response]);
  }

//single inventory view
  function inventoryItemView($req, $res, $args)  {
    $id = $args['id'];
    return $this->app->view->render($res, 'inventory/inventories.html', ['name'=>'list inventories', 'id'=>$id]);
  }

  function deleteView($req, $res, $args)  {
    $id = $args['id'];
    return $this->app->view->render($res, 'inventory/inventories.html', ['name'=>'Delete inventory', 'id'=>$id]);
  }

  function editView($req, $res, $args)  {
    $id = $args['id'];
    $inventoryQuery = "SELECT i.id, i.quantity, i.shop_id, p.product_name, p.product_type FROM inventory i INNER JOIN product p ON i.product_id = p.id WHERE i.id = :inventoryId";
    $statement = $this->conn->prepare($inventoryQuery);
    $statement->bindValue(':inventoryId', $id);
    $statement->execute();
    // $result = $stmt->fetch(PDO::FETCH_OBJ);
    $inventory = $statement->fetch(PDO::FETCH_OBJ);
    return $this->app->view->render($res, 'inventory/edit.html', ['name'=>'Update inventory', 'id'=>$id, 'inventory'=>$inventory]);
  }
}

?>
