<?php
  /**
   **My home controller
   **/
  class HomeController {
    private $app;
    function __construct($app){
      $this->app = $app;
    }

    function home($req, $res, $args)  {
      return $this->app->view->render($res, 'home.html', ['name'=>'This is the home page...Using twig template engine']);
    }
  }

 ?>
