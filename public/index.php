<?php
	session_start(); //required by slim flash messages by default
	use Respect\Validation\Validator as validator;
	require '../vendor/autoload.php';
	// require 'config/db.php';
	$settings = require 'settings.php';

	$app = new Slim\App($settings);

	//Session middleware
	// $app->add(new \Slim\Middleware\Session([
	// 'name' => 'petwana_session',
	// 'autorefresh' => true,
	// 'lifetime' => 60*60*1
	// ]));

//error messages middleware
	$app->add(function($req, $res, $next){
		$this->view->offsetSet('flash', $this->flash->getMessages());
		return $next($req, $res);
	});

// 	$app->add(function($request, $response, $next) {
//     $response = $next($request, $response);
//     return $response->withHeader('Content-Type', 'application/json');
// });

//validation
$usernameValidator = validator::alnum()->noWhitespace()->length(4, 50);
$passwordValidator = validator::alnum()->noWhitespace()->length(4, 50);

//check out on this !important
$validators = json_encode(array(
	'username' => $usernameValidator,
	'password' => $passwordValidator
));

// Register middleware for all routes
// If you are implementing per-route checks you must not add this
$app->add(new \DavidePastore\Slim\Validation\Validation($validators));

	require 'dependancies.php';
	require '../middleware/trailling.php';

	require 'api/userAPI.php';
	require 'api/productAPI.php';
	require 'api/inventoryAPI.php';
	require 'api/shopsAPI.php';
	require 'api/salesAPI.php';

	require 'routes/users.php';
	require 'routes/home.php';
	require 'routes/inventory.php';
	require 'routes/products.php';
	require 'routes/shops.php';
	require 'routes/sales.php';
	require 'routes/dashboard.php';

$app->run();
 ?>
