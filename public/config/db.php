<?php
class Database {
  private $dbname = 'shop_inv';
  private $user = 'root';
  private $pass = '';
  private $host = '127.0.0.1';

  function __construct(){}

  public function connect() {
    $connection_str = "mysql:host=$this->host;dbname=$this->dbname";
    $connection = new PDO($connection_str, $this->user, $this->pass);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $connection;
  }
}
?>
