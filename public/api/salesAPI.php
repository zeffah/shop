<?php
/**
*
*/
class SalesAPI{
  private $app;
  private $conn = null;
  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    // $this->inventoryAPI = new InventoryAPI($this->app);
  }

  function insertSales($saleData){
    try {
      $response = null;
      $data = json_decode($saleData);
      $query = "INSERT INTO sales (staff_id, shop_id, sale_amount) VALUES(:staff, :shop, :amount)";
      $stmt = $this->conn->prepare($query);
      $stmt->bindParam(':staff', $data->staffId);
      $stmt->bindParam(':shop', $data->shopId);
      $stmt->bindParam(':amount', $data->saleAmount);
      $stmt->execute();
      $rowsAffected  = $stmt->rowCount();
      if ($rowsAffected > 0) {
        $saleId = $this->conn->lastInsertId();
        $saleItems = $data->saleItems;
        foreach ($saleItems as $saleItem => $value) {
          $sql = "INSERT INTO sale_item (inventory_id, sale_id, selling_price, item_amount, quantity)
          VALUES(:inventory, :sale, :selling_price, :amount, :quantity)";
          $stmt = $this->conn->prepare($sql);
          $stmt->bindParam(':inventory', $value->inventoryId);
          $stmt->bindParam(':sale', $saleId);
          $stmt->bindParam(':amount', $value->itemAmount);
          $stmt->bindParam(':quantity', $value->itemQuantity);
          $stmt->bindParam(':selling_price', $value->sellingPrice);
          $stmt->execute();
          if ($stmt->rowCount() > 0) {
            $getQty = "SELECT quantity FROM inventory WHERE id = :id";
            $stmt = $this->conn->prepare($getQty);
            $stmt->bindParam(':id', $value->inventoryId);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            $quantity = $result->quantity;
            $newQty = $quantity-$value->itemQuantity;
            $inv = "UPDATE inventory SET quantity = :quantity WHERE id = :inventoryId AND shop_id = :shopId";
              $stmt = $this->conn->prepare($inv);
              $stmt->bindParam(':inventoryId', $value->inventoryId);
              $stmt->bindParam(':shopId', $data->shopId);
              $stmt->bindParam(':quantity', $newQty);
              $stmt->execute();
          }
        }
        $response = array('success' => 1, 'message'=> 'Sale created');
      }else {
        $response = array('success' => 0, 'message'=> 'Sale not created');
      }
      return json_encode($response);
    } catch (PDOException $e) {
      // $this->conn->rollback();
      return json_encode($e->getMessage());
    }

  }

  function getShopSales($shopId) {
    $response = array();
    try{
      $query = "SELECT sales.id, sales.sale_amount, sales.created_at, staff.staff_name, shop.shop_name FROM sales INNER JOIN staff ON sales.staff_id = staff.id INNER JOIN shop ON sales.shop_id = shop.id WHERE sales.shop_id = :shop_id";
      $stmt = $this->conn->prepare($query);
      $stmt->bindParam(':shop_id', $shopId);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($rows as $value => $row){
        $items = "SELECT sale_item.id, sale_item.quantity, sale_item.selling_price, sale_item.item_amount, product.product_name FROM sale_item INNER JOIN inventory ON sale_item.inventory_id = inventory.id INNER JOIN product ON inventory.product_id = product.id WHERE sale_id = :sale_id";
        $stmt = $this->conn->prepare($items);
        $stmt->bindParam(':sale_id', $row['id']);
        $stmt->execute();
        $saleItems = $stmt->fetchAll(PDO::FETCH_ASSOC);
        array_push($response, array('id'=>$row['id'],'sale_amount'=>$row['sale_amount'], 'staff'=>$row['staff_name'], 'shop_name'=>$row['shop_name'], 'created_at'=>$row['created_at'], 'sale_items'=>$saleItems));
      }
      return json_encode($response);
    }catch(PDOException $e){
      return json_encode($e->getMessage());
    }
  }

  function getSales($shopId) {
    $response = [];
    try{
      $query = "SELECT sales.id, sales.sale_amount, sales.created_at, staff.staff_name FROM sales INNER JOIN staff ON sales.staff_id = staff.id WHERE sales.shop_id = :shop_id";
      $stmt = $this->conn->prepare($query);
      $stmt->bindParam(':shop_id', $shopId);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($rows as $row){
        $items = "SELECT sale_item.id, sale_item.quantity, sale_item.selling_price, sale_item.item_amount, product.product_name FROM sale_item INNER JOIN inventory ON sale_item.inventory_id = inventory.id INNER JOIN product ON inventory.product_id = product.id WHERE sale_item.sale_id = :sale_id";
        $stment = $this->conn->prepare($items);
        $stment->bindParam(':sale_id', $row['id']);
        $stment->execute();
        $saleItems = $stment->fetchAll(PDO::FETCH_ASSOC);
        array_push($response, array('id'=>$row['id'],'sale_amount'=>$row['sale_amount'], 'staff'=>$row['staff_name'], 'created_at'=>$row['created_at'], 'sale_items'=>$saleItems));
      }
      return $response;
    }catch(PDOException $e){
      return json_encode($e->getMessage());
    }
  }

  function getShopsAndSales(){
    $shopsArray = [];
    try {
      $sql = "SELECT id, shop_name, location, postal_address, email_address, town,
      UNIX_TIMESTAMP(opening_hour) AS opening_hour, UNIX_TIMESTAMP(closing_hour) AS closing_hour,
      UNIX_TIMESTAMP(created_at) AS created_at FROM shop WHERE delete_status = :delete_status";
      $stmt = $this->conn->prepare($sql);
      $delete_status =  0;
      $stmt->bindParam(':delete_status', $delete_status, PDO::PARAM_INT);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        foreach ($rows as $shop) {
          $shopSales = $this->getShopSales($shop['id']);
          $mShopObject = array('id'=>$shop['id'], 'shop_name'=>$shop['shop_name'],
          'location'=>$shop['location'], 'postal_address'=>$shop['postal_address'], 'town'=>$shop['town'],
          'opening_hour'=>$shop['opening_hour'], 'closing_hour'=>$shop['closing_hour'], 'created_at'=>$shop['created_at'], 'sales'=>$shopSales);
          array_push($shopsArray, $mShopObject);
        }
        return json_encode(array('success'=> 1, 'message'=>'Successfully fetched shops',  'error'=>0, 'shops'=>$shopsArray));
      }
      return json_encode(array('message'=>'No shops found', 'error'=>1, 'success'=>0));
    } catch (PDOException $e) {
        return json_encode($e->getMessage());
    }
  }
}



?>
