<?php

/**
 *
 */
class ShopMobileAPI{
  private $shopAPI;
  private $app;
  private $conn = null;
  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->shopAPI = new ShopsAPI($this->app);
  }

  function fetchShops($req, $res, $args) {
      $data = $this->shopAPI->getAllShops();
      return $data;
  }

  function getShopAndInventories($req, $res, $args){
    $data = $this->shopAPI->getShopAndInventories();
  }
}


 ?>
