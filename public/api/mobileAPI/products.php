<?php

/**
 *
 */
class ProductsMobileAPI{
  private $productAPI;
  private $app;
  private $conn = null;

  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->productAPI = new ProductAPI($this->app);
  }

  function fetchProducts($req, $res, $args) {
      $data = $this->productAPI->listAllProducts();
      return $data;
  }

  function editProduct(){
    $data = file_get_contents("php://input");
    return $this->productAPI->editProduct($data);
  }

  function deleteProduct(){
    $data = file_get_contents("php://input");
    return $this->productAPI->deleteProduct($data);
  }

  function createNewProduct(){
    $data = file_get_contents("php://input");
    return $this->productAPI->createNewProduct($data);
  }
}
 ?>
