<?php
/**
 *
 */
class UserMobileAPi {
  private $userAPI;
  private $app;
  private $conn = null;
  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->userAPI = new UserAPI($this->app);
  }

  function login($req, $res, $args) {
    $data = $this->userAPI->userAunthenticate(json_encode($req->getParsedBody()));
    return $data;
  }

  function register($req, $res, $args) {
      $data = $this->userAPI->userCreate(json_encode($req->getParsedBody()));
      return $data;
  }

  function deleteUser($req, $res, $args) {
    $userId = $args['userId'];
    if(!is_numeric($userId)){
      return $res->withJson(array("success"=> 0, "error"=>"Unexpected data .Expected number for id. "));
    }
      $data = $this->userAPI->deleteUser($userId);
      return $data;
  }
}


 ?>
