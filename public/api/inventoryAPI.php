<?php

/**
 *
 */
class InventoryAPI {
  // public const INVENTORY_NOT_DELETED_STATUS = 0;
  // public const INVENTORY_DELETED_STATUS = 1;
  // public const INVENTORY_NOT_FOUND = 1;
  // public const INVENTORY_LIST_FOUND = 0;
  // public const INVENTORY_LIST_INVALID_SHOP_ID = 2;
  private $app;
  private $conn = null;
  function __construct($app) {
    $this->app = $app;
    $this->conn = $this->app->db;
  }

  function deleteInventory_($data){
    $data = json_decode($data);
    $id = $data->id;
    $deleteSql = "UPDATE inventory SET delete_status = :delete_status WHERE id = :id";
    $stmt = $this->conn->prepare($deleteSql);
    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':delete_status', 1);
    $result = $stmt->execute();
    if($result){
      return json_encode(['message'=>'Deleted successfully', 'success'=>1, 'id'=>$id]);
    }else{
      return json_encode(['message'=>'Deleted failed', 'success'=>0, 'id'=>$id]);
    }
  }

  function updateInventory($data) {
    $data = json_decode($data);
    $id = $data->id;
    $quantity = $data->quantity;
    $selling_price = $data->selling_price;
    $updateSql = "UPDATE inventory SET quantity = :quantity, selling_price = :selling_price WHERE id = :id";
    $stmt = $this->conn->prepare($updateSql);
    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':quantity', $quantity);
    $stmt->bindValue(':selling_price', $selling_price);
    $result = $stmt->execute();
    // $count = $stmt->rowCount();
    if($result){
      return json_encode(['message'=>'Updated successfully', 'success'=>1, 'data'=>$data]);
    }else{
      return json_encode(array('success'=> 0, 'message'=>'Nothing was updated'));
    }
  }

  function editInventory($data) {
    $data = json_decode($data);
    $id = $data->id;
    $quantity = $data->quantity;
    $curQty = $data->existing_quantity;
    $finalQty = $quantity + $curQty;
    $inventoryQuery = "SELECT i.id, i.quantity, p.product_name, p.product_type FROM inventory i INNER JOIN product p ON i.product_id = p.id WHERE i.id = :inventoryId";
    $statement = $this->conn->prepare($inventoryQuery);
    $statement->bindValue(':inventoryId', $id);
    $statement->execute();
    $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        $updateSql = "UPDATE inventory SET quantity = :quantity WHERE id = :id";
        $stmt = $this->conn->prepare($updateSql);
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':quantity', $finalQty);
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count > 0){
          return json_encode(['message'=>'Updated successfully', 'success'=>1, 'id'=>$id]);
        }
      }else{
        return json_encode(array('success'=> 0, 'message'=>'Inventory does not exist'));
      }
  }

  function deleteInventory($data){
    $data = json_decode($data);
    $id = $data->id;

    $inventoryQuery = "SELECT * FROM inventory i WHERE i.id = :inventoryId";
    $statement = $this->conn->prepare($inventoryQuery);
    $statement->bindValue(':inventoryId', $id);
    $statement->execute();
    $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        $deleteSql = "UPDATE inventory SET delete_status = :delete_status WHERE id = :id";
        $stmt = $this->conn->prepare($deleteSql);
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':delete_status', 1);
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count > 0){
          return json_encode(['message'=>'Deleted successfully', 'success'=>1, 'id'=>$id]);
        }else{
          return json_encode(['message'=>'Deleted failed', 'success'=>0, 'id'=>$id]);
        }
      }else{
        return json_encode(array('success'=> 0, 'message'=>'Inventory does not exist'));
      }
  }

  function createNewInventory($data) {
    $data = json_decode($data);
    try {
      if(!property_exists($data, "product_id")) {
        return json_encode(array('success'=> 0, 'message'=>'Product is not selected'));
      }

      if(!property_exists($data, "shop_id")) {
        return json_encode(array('success'=> 0, 'message'=>'Shop was not selected'));
      }

      $productId = $data->product_id;
      $shopId = $data->shop_id;
      $quantity = $data->quantity;
      $selling_price = $data->selling_price;

      $productQuery= "SELECT id FROM inventory WHERE product_id = :productId AND shop_id = :shopId";
      $statement = $this->conn->prepare($productQuery);
      $statement->bindValue(':productId', $productId);
      $statement->bindValue(':shopId', $shopId);
      $statement->execute();
      $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        return json_encode(array('success'=> 0, 'message'=>'Selected product already exist in inventory'));
      }
      $sql = "INSERT INTO inventory (product_id, shop_id, quantity, sale_count, selling_price, delete_status)
      VALUES (:product_id, :shop_id, :quantity, :sale_count, :selling_price, :delete_status)";
      $stmt = $this->conn->prepare($sql);
      $deleteStatus = 0;
      $saleCount = 0;
      $stmt->bindParam(':product_id', $productId);
      $stmt->bindParam(':shop_id', $shopId);
      $stmt->bindParam(':sale_count', $saleCount);
      $stmt->bindParam(':selling_price', $selling_price);
      $stmt->bindParam(':delete_status', $deleteStatus);
      $stmt->bindParam(':quantity', $quantity);
      $stmt->execute();
      $rowsAffected  = $stmt->rowCount();
      if ($rowsAffected > 0) {
        return json_encode(array('success'=> 1, 'message'=>'created successfully'));
      }else {
        return json_encode(array('success'=> 0, 'message'=>'insertion error'));
      }
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function listShopInventories($shopId){
    try {
      $sql = "SELECT inventory.id as inventory_id, inventory.shop_id, inventory.quantity, inventory.sale_count,
      product.product_name, product.product_manufacturer, product.product_type, product.buying_price,
      product.selling_price, shop.shop_name, shop.town, shop.email_address, shop.postal_address, shop.location,
      inventory.product_id, UNIX_TIMESTAMP(inventory.created_at) AS created_at, UNIX_TIMESTAMP(inventory.updated_at) AS updated_at
      FROM inventory INNER JOIN product ON inventory.product_id = product.id INNER JOIN shop ON inventory.shop_id = shop.id
      WHERE inventory.delete_status = :delete_status AND inventory.shop_id = :shop_id";
      $stmt = $this->conn->prepare($sql);
      $delete_status = 0;
      $stmt->bindParam(':delete_status', $delete_status, PDO::PARAM_INT);
      $stmt->bindParam(':shop_id', $shopId, PDO::PARAM_INT);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        return json_encode(array('success'=> 1, 'error'=> 0, 'inventories'=>$rows));
      }
      return json_encode(array('message'=>'inventory products not found', 'error'=> 1, 'success'=>0));
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function fetchShopInventories($shopId){
    $inventoryArray = [];
    try {
      $sql = "SELECT inventory.id as inventory_id, inventory.shop_id, inventory.quantity, inventory.sale_count,
      product.product_name, product.product_manufacturer, product.product_type, product.buying_price,
      inventory.selling_price, inventory.product_id, UNIX_TIMESTAMP(inventory.created_at) AS created_at, UNIX_TIMESTAMP(inventory.updated_at) AS updated_at
      FROM inventory INNER JOIN product ON inventory.product_id = product.id WHERE inventory.delete_status = :delete_status AND inventory.shop_id = :shop_id";
      $stmt = $this->conn->prepare($sql);
      $delete_status = 0;
      $stmt->bindParam(':delete_status', $delete_status, PDO::PARAM_INT);
      $stmt->bindParam(':shop_id', $shopId, PDO::PARAM_INT);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        return $rows;
      }
      return json_encode(array('message'=>'inventory products not found', 'error'=> 1, 'success'=>0));
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }
}
 ?>
