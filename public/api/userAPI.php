<?php
class UserAPI {
  private $app;
  private $conn = null;

  function __construct($app) {
    $this->app = $app;
    $this->conn = $this->app->db;
  }

  function userAunthenticate($data) {
      // return $data;
    $data = json_decode($data);
    try {
      $sql = "SELECT * FROM staff WHERE username = :username AND password = :password LIMIT 1";
      $stmt = $this->conn->prepare($sql);

      $stmt->bindParam(':username',  $data->username);
      $stmt->bindParam(':password',  $data->password);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        return json_encode(array('success'=> 1, 'message'=>'login successful', 'user'=>$rows[0]));
      }
      return json_encode(array('error'=>'Wrong username or password', 'message'=>'Wrong username or password', 'success'=>0, 'user'=>(object)[]));
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function userCreate($parsedData){
    $data = json_decode($parsedData);
    $response = null;
    try {
      $sql = "INSERT INTO staff (username, password, phone_number, staff_name, email_address, national_id, shop_id)
      VALUES(:username, :password, :phone_number, :staff_name, :email_address, :national_id, :shop_id)";
        $stmt = $this->conn->prepare($sql);
        if(!property_exists ($data , 'shop_id') || !$data->shop_id){
          return json_encode(array('error'=>'Shop not selected', "success"=>0));
        }

        $staff_name = $data->first_name." ".$data->last_name;

        $stmt->bindParam(':username', $data->first_name);
        $stmt->bindParam(':password', $data->last_name);
        $stmt->bindParam(':phone_number', $data->phone_number);
        $stmt->bindParam(':staff_name', $staff_name);
        $stmt->bindParam(':email_address', $data->email_address);
        $stmt->bindParam(':national_id', $data->national_id);
        $stmt->bindParam(':shop_id', $data->shop_id);
        $stmt->execute();
        if ($stmt) {
          $response = array('success' => 1, 'message'=> "Registration successful. Username is '$data->first_name' and password is '$data->last_name@petwana'. Use these credetials to login to petwana mobile application only.");
        }else {
          $response = array('success' => 0, 'message'=> 'Registration failed');
        }
      return json_encode($response);
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function deleteUser($userId) {
    $response = null;
    try {
      $sql = "DELETE FROM staff WHERE id = :user_id";
      $stmt = $this->conn->prepare($sql);
      $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
      $stmt->execute();
      $rowsAffected  = $stmt->rowCount();
      if ($rowsAffected > 0) {
         $response = array('success' => 1, 'message'=> 'Delete user passed');
      }else {
         $response = array('success' => 0, 'message'=> 'No use with that id was found');
      }
      return json_encode($response);
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }

  }

}
 ?>
