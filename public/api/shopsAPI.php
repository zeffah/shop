<?php
class ShopsAPI {
  private $app = null;
  private $conn = null;
  private $inventoryAPI;
  private $salesAPI;

  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->inventoryAPI = new InventoryAPI($this->app);
    $this->salesAPI = new SalesAPI($this->app);
  }

  function getShopAndInventories(){
    try {
      $sql = "SELECT id, shop_name, location, postal_address, email_address, town, UNIX_TIMESTAMP(opening_hour) AS opening_hour, UNIX_TIMESTAMP(closing_hour) AS closing_hour, UNIX_TIMESTAMP(created_at) AS created_at FROM shop WHERE delete_status = :delete_status";
      $stmt = $this->conn->prepare($sql);
      $delete_status =  0;
      $stmt->bindParam(':delete_status', $delete_status, PDO::PARAM_INT);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        return json_encode(array('success'=> 1, 'message'=>'Successfully fetched shops',  'error'=>0, 'shops'=>$rows));
      }
      return json_encode(array('message'=>'No shops found', 'error'=>1, 'success'=>0));
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }

  }

  function getAllShops(){
    $shopsArray = [];
    try {
      $sql = "SELECT id, shop_name, location, postal_address, email_address, town,
      UNIX_TIMESTAMP(opening_hour) AS opening_hour, UNIX_TIMESTAMP(closing_hour) AS closing_hour,
      UNIX_TIMESTAMP(created_at) AS created_at FROM shop WHERE delete_status = :delete_status";
      $stmt = $this->conn->prepare($sql);
      $delete_status =  0;
      $stmt->bindParam(':delete_status', $delete_status, PDO::PARAM_INT);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        foreach ($rows as $shop) {
          $shopInventory = $this->inventoryAPI->fetchShopInventories($shop['id']);
          $shopSales = $this->salesAPI->getSales($shop['id']);
          $mShopObject = array('id'=>$shop['id'], 'shop_name'=>$shop['shop_name'],
          'location'=>$shop['location'], 'postal_address'=>$shop['postal_address'], 'town'=>$shop['town'],
          'opening_hour'=>$shop['opening_hour'], 'closing_hour'=>$shop['closing_hour'], 'created_at'=>$shop['created_at'], 'inventory'=>$shopInventory, 'salesList'=>$shopSales);
          array_push($shopsArray, $mShopObject);
        }
        return json_encode(array('success'=> 1, 'message'=>'Successfully fetched shops',  'error'=>0, 'shops'=>$shopsArray));
      }
      return json_encode(array('message'=>'No shops found', 'error'=>1, 'success'=>0));
    } catch (PDOException $e) {
        return json_encode($e->getMessage());
    }
  }

  function getShopInfo($shopId){
    try{
      $sql = "SELECT * FROM shop WHERE id = :shopId AND delete_status = :delete_status";
      $stmt = $this->conn->prepare($sql);
      $delete_status =  0;
      $stmt->bindParam(':delete_status', $delete_status, PDO::PARAM_INT);
      $stmt->bindParam(':shopId', $shopId, PDO::PARAM_INT);
      $stmt->execute();
      $result = $stmt->fetch(PDO::FETCH_OBJ);
      if($result){
        return json_encode(array('success'=>1, 'shop'=>$result));
      }else{
        return json_encode(array('message'=>'No shop found', 'error'=>1, 'success'=>0));
      }
    }catch(PDOException $e){
      return json_encode($e->getMessage());
    }
  }

  function addShop($data){
    $data = json_decode($data);
    try {
      $sql = "INSERT INTO shop (shop_name, location, town, postal_address, email_address)
    VALUES(:shop_name, :shop_loc, :town, :postal, :email)";
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':shop_name', $data->shop_name);
      $stmt->bindParam(':shop_loc', $data->shop_location);
      $stmt->bindParam(':town', $data->town);
      $stmt->bindParam(':postal', $data->postal_address);
      $stmt->bindParam(':email', $data->email_address);
      $stmt->execute();
      $rowsAffected  = $stmt->rowCount();
      if ($rowsAffected > 0) {
        return json_encode(array('success'=> 1, 'message'=>'created successfully'));
      }else {
        return json_encode(array('success'=> 0, 'message'=>'insertion error'));
      }
    }catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }
}
 ?>
