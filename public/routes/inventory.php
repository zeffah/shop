<?php
require '../controller/inventory.php';
require 'api/mobileAPI/inventory.php';

$app->group('', function(){
  $this->post('/inventoryAPI/list/{shop_id}', \InventoryMobileAPI::class.':fetchInventories');
  $this->post('/inventoryAPI/update', \InventoryMobileAPI::class .':updateInventory');
  $this->post('/inventoryAPI/delete', \InventoryMobileAPI::class .':deleteInventory');
  $this->post('/inventoryAPI/create', \InventoryMobileAPI::class .':createInventory');

})->add(function($request, $response, $next) {
  $response = $next($request, $response);
  return $response->withHeader('Content-Type', 'application/json')
  ->withHeader('Access-Control-Allow-Origin', '*');
});

$app->group('/inventories', function() {
  $this->get('/shop/{shop_id}', \InventoryController::class .':showAllView');
  $this->get('/add', \InventoryController::class.':createView');
  $this->get('/single/{id}',  \InventoryController::class .':inventoryItemView');
  $this->get('/{id}/edit', \InventoryController::class .':editView');

  $this->post('/inventoryAPI/createNew', \InventoryController::class.':createInventory');
  $this->post('/inventoryAPI/delete', \InventoryController::class .':deleteInventory');
  $this->post('/inventoryAPI/edit', \InventoryController::class .':editInventory');
  // $this->post('/inventoryAPI/update/{id}', \InventoryController::class .':updateInventory');
});
?>
