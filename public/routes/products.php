<?php
require '../controller/products.php';
require 'api/mobileAPI/products.php';
//Internal consumption
$app->group('/', function(){
  $this->get('products', \ProductController::class .':getAllProducts');
  $this->get('products/createNew', \ProductController::class.':newView');
  $this->post('products/productAPI/create', \ProductController::class.':createNewProduct');
});

//For external consumption
$app->group('', function(){
  $this->get('/productsAPI/list', \ProductsMobileAPI::class .':fetchProducts');
  $this->post('/productsAPI/update', \ProductsMobileAPI::class.':editProduct');
  $this->post('/productsAPI/delete', \ProductsMobileAPI::class.':deleteProduct');
  $this->post('/productsAPI/createNew', \ProductsMobileAPI::class.':createNewProduct');
})->add(function($request, $response, $next) {
  $response = $next($request, $response);
  return $response->withHeader('Content-Type', 'application/json')
  ->withHeader('Access-Control-Allow-Origin', '*');
});
