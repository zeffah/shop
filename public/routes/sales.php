<?php
// require '../controller/users.php';
require 'api/mobileAPI/sales.php';

//mobile and external consumer apis
$app->group('/', function() {
  $this->post('salesAPI/createSale', \SalesMobileAPi::class.':createSale');
  $this->get('salesAPI/sales', \SalesMobileAPI::class.':fetchShopsAndSales');
})->add(function($request, $response, $next) {
  $response = $next($request, $response);
  return $response->withHeader('Content-Type', 'application/json')
  ->withHeader('Access-Control-Allow-Origin', '*');
});
