<?php
require '../controller/users.php';
require 'api/mobileAPI/users.php';

//mobile and external consumer apis
$app->group('', function() {
  $this->post('/usersAPI/login', \UserMobileAPi::class.':login');
  $this->post('/usersAPI/register', \UserMobileAPi::class.':register');
  $this->delete('/usersAPI/delete/{userId}', \UserMobileAPi::class.':deleteUser');
})->add(function($request, $response, $next) {
  $response = $next($request, $response);
  return $response->withHeader('Content-Type', 'application/json');
});

  //internal consumer apis
$app->group('/users', function() {
  $this->get('', \UsersController::class .':getAllUsers');
  $this->get('/userlogin', \UsersController::class.':userLogin');
  $this->get('/user-register', \UsersController::class.':userRegister');
  $this->post('/login',  \UsersController::class .':login');
  $this->post('/register', \UsersController::class .':register');
});
?>
