<?php
require '../controller/shops.php';
require 'api/mobileAPI/shops.php';

//mobile and external consumer apis
$app->group('', function() {
  $this->get('/shopsAPI/shops', \ShopMobileAPi::class.':fetchShops');
})->add(function($request, $response, $next) {
  $response = $next($request, $response);
  return $response->withHeader('Content-Type', 'application/json')
  ->withHeader('Access-Control-Allow-Origin', '*');
});

//internal consume api
$app->group('/shops', function() {
  $this->get('', \ShopsController::class.':getAllShops');
  $this->get('/addShop', \ShopsController::class.':addShop');
  $this->get('/{shopId}', \ShopsController::class.':shopInfo');
  $this->post('/shopsAPI/create', \ShopsController::class.':createNew');
});
 ?>
