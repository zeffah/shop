<?php
require '../controller/dashboard.php';

  //internal consumer apis
  $app->group('/', function() {
    $this->get('dashboard', \DashboardController::class .':dashboard');
  });
?>
